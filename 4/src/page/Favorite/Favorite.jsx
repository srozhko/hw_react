import { AiTwotoneStar } from "react-icons/ai";
import Button from "../../components/Button";

import './Favorite.scss'

const Favorite =({favorite,openModal, increaseProduct, toogleFavorit})=>{
    // console.log('fav page',favorite);

    const favCardRender = favorite.map((card) => {
        const {id, name, price, color, img, activeStar} = card
        return(
        <div className="card" id={id}>
            <div className="img-wrap">
                <img src={img} alt={name} />    
            </div>
            <AiTwotoneStar className={activeStar && "star active"} onClick={()=>toogleFavorit(card)}/>
            <div className="product_name">
                <div className="name">{name}</div>
                <div className="price">{price}$</div> 
            </div>
            <div className="color">Color: {color}</div>
            <Button text ="Додати в кошик" backgroundColor="rgb(11, 73, 159)" onClick={()=> openModal(card)} />
        </div> 
        )
    })
    // console.log(favCardRender);
    return(
        <div className="busket-wrapper">
            <div className="title-wrap">
                <h1 className="title">Выбраные тoвары</h1>
            </div>
            <div className="content-wrap">
            {favCardRender}
            </div>
        </div>
    )
}

export default Favorite;