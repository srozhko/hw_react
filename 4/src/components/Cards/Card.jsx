import React, { useState, useEffect, Component } from "react";
import { AiTwotoneStar } from "react-icons/ai";
import Button from "../Button";


const Card =({card, openModal, increaseProduct, toogleFavorit}) => {
    const {id, name, price, color, img, activeStar} = card;
    const [Star, setStar] = useState(false)

    return (
        <div className="card" id={id}>
            <div className="img-wrap">
                <img src={img} alt={name} />    
            </div>
            <AiTwotoneStar className={activeStar ? "star active":"star"} onClick={()=>{toogleFavorit(card); setStar(!activeStar)}}/>
            <div className="product_name">
                <div className="name">{name}</div>
                <div className="price">{price}$</div> 
            </div>
            <div className="color">Color: {color}</div>
            <Button text ="Додати в кошик" backgroundColor="rgb(11, 73, 159)" onClick={()=>openModal(card)} />
        </div>
    )
}

export default Card;