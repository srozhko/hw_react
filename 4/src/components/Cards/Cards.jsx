import Card from "./Card";
import './Cards.scss'

const Cards = ({products, openModal, increaseProduct, toogleFavorit}) => {

    const cardRender = products.map((card, index) => <Card key={index} card={card} openModal={openModal} increaseProduct={increaseProduct} toogleFavorit={toogleFavorit}/>)

    return (
        <>
            <div className="cards">
                {cardRender}
            </div> 
        </>
    )
}

export default Cards;