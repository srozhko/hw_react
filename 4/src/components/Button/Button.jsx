import PropsTypes from 'prop-types'
import './Button.scss'

const Button =({text, onClick, backgroundColor, type}) =>{
  return (
    <button className="button" type={type} onClick={onClick} style={{backgroundColor:backgroundColor}} >{text}</button>
  )
}
 Button.propsTypes = {
  text: PropsTypes.string,
  onClick: PropsTypes.func,
  type: PropsTypes.string,
  backgroundColor: PropsTypes.string,
 }
Button.defaultProps ={
  type: 'button'
}

export default Button;