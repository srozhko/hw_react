import { BsBasket } from "react-icons/bs";
// import { AiTwotoneStar } from "react-icons/ai";
import { BsStar } from "react-icons/bs"
import { BiHome } from 'react-icons/bi'
import {Link} from "react-router-dom"

import './Header.scss'

const Header = ({counterStarProps, counterProductProps}) => {

    return (
        <header className='header'>
            <div className="header_logo">
                <img src="./img/logo/logo.png" alt="" />
            </div>
            <nav className="header_menu">
                <ul className="header_wrapper">
                    <li className="header_item">
                        <Link to={'/'}>
                            <BiHome className="icon"/>
                        </Link>
                    </li>
                    <li className="header_item">
                        <Link to={'/busket'}>
                            <BsBasket className="icon"/>
                        </Link>
                        { counterProductProps>0 && <div className='counter bus'>{counterProductProps}</div>}        
                    </li>
                    <li className="header_item">
                        <Link to={'/favorite'}>
                            <BsStar className="icon"/>
                        </Link>
                        {counterStarProps>0 && <div className='counter fav'>{counterStarProps}</div>} 
                    </li>
                </ul>
            </nav>
        </header>
    )
}   

export default Header;

