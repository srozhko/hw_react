// import React, { Component } from "react";
import './Modal.scss'
import Button from "../Button/Button";

// class Modal extends Component {

//     render() {
//         const {header, closeButton, text, actionOk, actionCen } =this.props
//         return (
//             <div className="modal-wrapper" onClick={closeButton}>
//                 <div className="modal-content" onClick={(e) => e.stopPropagation()}>
//                     <div className="modal-header">
//                         <div className="header-title">{header}</div>
//                         <div className="button-wrap">
//                             <button className="close-btn" onClick={closeButton}>
//                                 <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="xMinYMin">
//                                     <path xmlns="http://www.w3.org/2000/svg" d="M 4.1113281 1.2832031 L 1.2832031 4.1113281 L 5.171875 8 L 1.2832031 11.888672 L 4.1113281 14.716797 L 8 10.828125 L 11.888672 14.716797 L 14.716797 11.888672 L 10.828125 8 L 14.716797 4.1113281 L 11.888672 1.2832031 L 8 5.171875 L 4.1113281 1.2832031 z"/> 
//                                 </svg>
//                             </button>
//                         </div>
//                     </div>
//                     <div className="modal-text">
//                         <p className="text">{text}</p>
//                     </div>
//                     <div className="modal-btn">
//                         <Button text ="Ok" backgroundColor = "red" onClick={actionOk}/>
//                         <Button text ="Cancel" backgroundColor = "red" onClick={actionCen}/>
//                     </div>
//                 </div>
//             </div>
//         )
//     }
// }

const Modal =({modalActive, setModalActive, header, children, action}) =>{
    return (
        <div className={modalActive ? "modal-wrapper active": "modal-wrapper"} onClick={()=>setModalActive(false)}>
            <div className={modalActive ? "modal-content active": "modal-content"} onClick={e=>e.stopPropagation()}>
                <div className="modal-header">
                    <div className="header-title">{header}</div>
                    <div className="button-wrap">
                        <button className="close-btn" onClick={()=>setModalActive(false)}>
                            <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="xMinYMin">
                                <path xmlns="http://www.w3.org/2000/svg" d="M 4.1113281 1.2832031 L 1.2832031 4.1113281 L 5.171875 8 L 1.2832031 11.888672 L 4.1113281 14.716797 L 8 10.828125 L 11.888672 14.716797 L 14.716797 11.888672 L 10.828125 8 L 14.716797 4.1113281 L 11.888672 1.2832031 L 8 5.171875 L 4.1113281 1.2832031 z"/> 
                            </svg>
                        </button>
                    </div>
                </div>
                <div className="modal-text">
                    <p className="text">{children}</p>
                </div>
                <div className="modal-btn">
                    <Button text ="Ok" backgroundColor = "blue" onClick={action}/>
                    <Button text ="Cancel" backgroundColor = "blue" onClick={()=> setModalActive(false)}/>
                </div>
            </div>
        </div>
    )
}
export default Modal;