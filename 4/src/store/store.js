import {configureStore} from '@reduxjs/toolkit'
import logger from 'redux-logger'


export default store = configureStore({
    reducer: {
        products: productReducer
    }
})