import React, {useState , useEffect } from "react";
import {Routes, Route} from 'react-router-dom';

import Header from "./components/Header";
import Footer from "./components/Footer";
import Cards from "./components/Cards";
import Busket from "./page/Busket";
import Favorite from "./page/Favorite";
import Modal from "./components/Modal";


import sendRequest from "./helpers/sendRequest";
import './app.scss'


const App = () => { 
  const [products, setProducts] = useState([])
  const [modalActiveFirst, setModalActiveFirst] = useState(false)
  const [modalActiveSecond, setModalActiveSecond] = useState(false) 
  const [busket, setBusket] = useState([])
  const [favorite, setFavorite] = useState([])
  const [infoCart, setInfoCart] = useState({})

  useEffect(()=>{
    sendRequest('./product.json')
    .then (data=>{
        console.log("data", data);
        setProducts(data)
        console.log("products",products);
    })
  }, [])
  // console.log("products",products);

  const openModalAdd = (card) => {
    setModalActiveFirst(true)
    const infoproduct = card;
    // console.log('infocard', infoproduct);
    setInfoCart(infoproduct)
  }

  const increaseProduct = () => {
    // const newcart = infoCart
    const cartArray = [...busket, infoCart]
    setBusket(cartArray)
    localStorage.setItem('busketcart', JSON.stringify(cartArray))
    setModalActiveFirst(false)
  }

  const openModalDel = (card) => {
    setModalActiveSecond(true)
    const infoproduct = card;
    console.log('infocard', infoproduct);
    setInfoCart(infoproduct)
  }
  
  const decreaseProduct = () => {
    const delProduct = infoCart
    const busketProduct = busket.filter(product => product.id != delProduct.id)
    // console.log(busProduct);
    setBusket(busketProduct)
    localStorage.setItem('busketcart', JSON.stringify(busketProduct))
    setModalActiveSecond(false)
  }
  
  const toogleFavorit = (product) => {    
    let filterProduct = products.map(card => {
      if (card.id === product.id) {
        if(!card.activeStar) {
          card.activeStar = true;
        } else card.activeStar = false;
      }
      return card
    })
    
    let favcard = filterProduct.filter(card=> card.activeStar ? card : null)
    // let favArray = [...favorite, favcart]
    setFavorite(favcard)
    setProducts(filterProduct)
    localStorage.setItem('favcarts', JSON.stringify(favcard))
    // console.log('filterProduct',filterProduct);
  }
  

  return (
    <>
      <Header counterProductProps = {busket.length} counterStarProps = {favorite.length}/>
      <main className="container">
        <Routes>
          <Route index element={<Cards products={products} openModal={openModalAdd} increaseProduct={increaseProduct} toogleFavorit={toogleFavorit}/>}/> 
          <Route path="/busket" element={<Busket busket={busket} openModal={openModalDel}/>}/>
          <Route path="/favorite" element={<Favorite favorite={favorite} openModal={openModalAdd}  toogleFavorit={toogleFavorit}/>}/>
        </Routes>  
      </main>
      <Footer/>

      {modalActiveFirst && <Modal modalactive = {modalActiveFirst} setModalActive={setModalActiveFirst} header ="Добавить товар в корзину?" action={increaseProduct}/>}
      {modalActiveSecond && <Modal modalactive = {modalActiveSecond} setModalActive={setModalActiveSecond} header ="Удалить товар из корзины?" action={decreaseProduct}/>}
    </>
  )
}

export default App;
