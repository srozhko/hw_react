import React, { Component } from "react";
import PropsTypes from 'prop-types'
import './Button.scss'

class Button extends Component {
   
  render() {
    const {text, onClick, backgroundColor} =this.props
    return (
      <button className="button" onClick={onClick} style={{backgroundColor:backgroundColor}} >{text}</button>
    )
  }
}

// const Button =({text, onClick, backgroundColor, type}) =>{ 
//   return (
//     <button className="button" type={type} onClick={onClick} style={{backgroundColor:backgroundColor}} >{text}</button>
//   )
// }
//  Button.propsTypes = {
//   text: PropsTypes.string,
//   onClick: PropsTypes.func,
//   type: PropsTypes.string,
//   backgroundColor: PropsTypes.string,
//  }
// Button.defaultProps ={
//   type: 'button'
// }

export default Button;