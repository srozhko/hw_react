import React, { Component } from "react";
import './Modal.scss'
import Button from "../Button/Button";

class Modal extends Component {

    render() {
        const {header, closeButton, text, action} =this.props
        return (
            <div className="modal-wrapper" onClick={closeButton}>
                <div className="modal-content" onClick={(e) => e.stopPropagation()}>
                    <div className="modal-header">
                        <div className="header-title">{header}</div>
                        <div className="button-wrap">
                            <button className="close-btn" onClick={closeButton}>
                                <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="xMinYMin">
                                    <path xmlns="http://www.w3.org/2000/svg" d="M 4.1113281 1.2832031 L 1.2832031 4.1113281 L 5.171875 8 L 1.2832031 11.888672 L 4.1113281 14.716797 L 8 10.828125 L 11.888672 14.716797 L 14.716797 11.888672 L 10.828125 8 L 14.716797 4.1113281 L 11.888672 1.2832031 L 8 5.171875 L 4.1113281 1.2832031 z"/> 
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="modal-text">
                        <p className="text">{text}</p>
                    </div>
                    <div className="modal-btn">
                        <Button text ="Ok" backgroundColor = "red" onClick={action}/>
                        <Button text ="Cancel" backgroundColor = "red" onClick={action}/>
                    </div>
                </div>
            </div>
        )
    }
}

// const Modal =({active, setActive, header, closeButton, text, action}) =>{
//     return (
//         <div className={active ? "modal-wrapper active": "modal-wrapper"} onClick={()=>setActive(false)}>
//             <div className={active ? "modal-content active": "modal-content"} onClick={e=>e.stopPropagation()}>
//                 <div className="modal-header">
//                     <div className="header-title">{header}</div>
//                     <div className="close-btn" onClick={()=>setActive(false)}>
                        // {/* <button class="delete"> */}
                        //     <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="xMinYMin">
                        //         <path xmlns="http://www.w3.org/2000/svg" d="M 4.1113281 1.2832031 L 1.2832031 4.1113281 L 5.171875 8 L 1.2832031 11.888672 L 4.1113281 14.716797 L 8 10.828125 L 11.888672 14.716797 L 14.716797 11.888672 L 10.828125 8 L 14.716797 4.1113281 L 11.888672 1.2832031 L 8 5.171875 L 4.1113281 1.2832031 z"/> 
                        //     </svg>
                        // {/* </button> */}
//                         {/* {closeButton} */}
//                     </div>
//                 </div>
//                 <div className="modal-text">
//                     <p className="text">{text}</p>
//                 </div>
//                 <div className="modal-btn">
//                     <Button text ="Ok" backgroundColor = "red" onClick={()=> console.log("ok")}/>
//                     <Button text ="Cancel" backgroundColor = "red" onClick={()=> setActive(false)}/>
//                 </div>
//             </div>
//         </div>
//     )
// }
export default Modal;