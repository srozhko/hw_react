import React, { Component, useState } from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import './app.scss'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    openOne: false,
    openTwo : false
  }
  }

  
  showModalFirst = ()=> {
    this.setState({openOne:true, openTwo:false})
  }

  showModalSecond =() => {
    this.setState({openOne:false, openTwo: true})
  }

  closeModal = ()=> {
    this.setState({openOne:false, openTwo: false})  
  }

  render() {
    return (
      <>
        <div className="wrapper-btn">
          <Button text ="Open first modal" backgroundColor="red" onClick={this.showModalFirst}/>
          <Button text ="Open second modal"backgroundColor="blue" onClick={this.showModalSecond}/>
        </div>

        {this.state.openOne && <Modal header ="Do you want to delete this file?" text ="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?" action = {this.closeModal} closeButton = {this.closeModal}/> }
        {this.state.openTwo && <Modal header ="Do you want to replace this file?" text ="If you replace this file, it won't be possible to find in this package. Are you sure you want to replace it?" action = {this.closeModal} closeButton = {this.closeModal}/> }
      </>
    )
  }
}

// const App = () => {
//   const [modalActiveFirst, setModalActiveFirst] = useState(false)
//   const [modalActiveSecond, setModalActiveSecond] = useState(false)

//   return (
//     <>
//       <main>
//         <div className="wrapper-btn">
//           <Button text ="Open first modal" backgroundColor="red" onClick={()=>setModalActiveFirst(true)}>Open first modal</Button>
//           <Button text ="Open second modal" backgroundColor="blue" onClick={()=>setModalActiveSecond(true)}>Open second modal</Button>
//         </div>
//       </main>
//       <Modal active = {modalActiveFirst} setActive={setModalActiveFirst} header ="Do you want to delete this file?" text ="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"/>
//       <Modal active = {modalActiveSecond} setActive={setModalActiveSecond} header ="Do you want to replace this file?" text ="If you replace this file, it won't be possible to find in this package. Are you sure you want to replace it?"/>

//     </>
//   )
// }

export default App;
