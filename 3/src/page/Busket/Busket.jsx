import'./Busket.scss'

const Busket =({busket, openModal})=>{
    console.log(busket);
    const busCardRender = busket.map((card) => {
        const {id, name, price, color, img, activeStar} = card
        return(
        <div className="card" id={id}>
            <div className="img-wrap">
                <img src={img} alt={name} />    
            </div>
            <button className="delete-btn" onClick={()=>openModal(card)}>
                <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="xMinYMin">
                    <path xmlns="http://www.w3.org/2000/svg" d="M 4.1113281 1.2832031 L 1.2832031 4.1113281 L 5.171875 8 L 1.2832031 11.888672 L 4.1113281 14.716797 L 8 10.828125 L 11.888672 14.716797 L 14.716797 11.888672 L 10.828125 8 L 14.716797 4.1113281 L 11.888672 1.2832031 L 8 5.171875 L 4.1113281 1.2832031 z"/> 
                </svg>
            </button>
            <div className="product_name">
                <div className="name">{name}</div>
                <div className="price">{price}$</div> 
            </div>
            <div className="color">Color: {color}</div>
        </div> 
        )
    })

    return(
        <>
            <div className="busket-wrapper">
                <div className="title-wrap">
                    <h1 className="title">Корзина тoваров</h1>
                </div>
                <div className="content-wrap">
                   {busCardRender}
                </div>
            </div>
        </>
    )
}

export default Busket;