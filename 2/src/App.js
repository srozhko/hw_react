import React, { Component, useState } from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Cards from "./components/Cards";
import Modal from "./components/Modal";
import sendRequest from "./Helpers";

import './app.scss'


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      openModal: false,
      cards:[],
      busket:[],
      favorite:[],  
      infocard:{}    
    }
  }

  componentDidMount = () => {
    sendRequest('./product.json')
    .then ( data =>{
        this.setState ({
            cards: data
        })
        // console.log(this.state.cards);
    }, [])
  }
  
  toogleStar= (product) => {
    let filterProduct = this.state.cards.map(card => {
      if (card.id === product.id) {
        if(!card.activeStar) {
          card.activeStar = true;
        } else card.activeStar = false;
      }
      return card
    })
    
    let favcard = filterProduct.filter(card=> card.activeStar ? card : null)
    this.setState({
        favorite: favcard
    })
    this.setState({
      cards: filterProduct
    })
    localStorage.setItem('favcarts', JSON.stringify(favcard))
    // console.log('filterProduct',filterProduct);
    // console.log(this.state.favorite.length);
  }

  increaseProduct= () => {
    const product = this.state.infocard
    const cartArray = [...this.state.busket, product]
    this.setState({
      busket: [...this.state.busket,product]
    })
    localStorage.setItem('busketcart', JSON.stringify(cartArray))
    this.setState({
      openModal:false
    })
  }

  showModal = (card)=> {
    this.setState({openModal:true})
    const product = card;
    console.log('product',product);
    this.setState({infocard:product})
    // console.log('infocard',this.state.infocard);
  }
  
  closeModal = ()=> {
    this.setState({openModal:false})  
  }

  render() {
    const {cards} = this.state
    return (
      <>
        <Header counterProductProps = {this.state.busket.length} counterStarProps = {this.state.favorite.length}/>
        <Cards cards={cards} showModal={this.showModal} toogleStar ={this.toogleStar}/>
        <Footer/>

        {this.state.openModal && <Modal header ="Do you add this product for your basket?" text ="Yes or no?" actionCen = {this.closeModal} actionOk={this.increaseProduct} closeButton = {this.closeModal}/> }
      </>
    )
  }
}

export default App;