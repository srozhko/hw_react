import React, { Component } from 'react';
import { BsBasket } from "react-icons/bs";
import { AiTwotoneStar } from "react-icons/ai";

import './Header.scss'

class Header extends Component {

    render() {
        const {counterStarProps, counterProductProps} = this.props;
        
        return(
            <header className='header'>
                <div className="header_logo">
                    <img src="./img/logo/logo.png" alt="" />
                </div>
                <div className='header-icon'>
                    <div className="select">
                        <AiTwotoneStar className="select-button"/>
                        {counterStarProps>0 && <div className='counter'>{counterStarProps}</div>}        
                    </div>
                    <div className="basket">
                        <BsBasket className="shop-cart-button"/>
                        { counterProductProps>0 && <div className='counter'>{counterProductProps}</div>}        
                    </div>
                </div>
            </header> 
        )}
}

export default Header;

