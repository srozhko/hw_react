import { Component } from 'react';
import './Footer.scss'

class Footer extends Component {
    render() {
        return (
            <footer className='footer'>
                <p className='footer_text'>сайт разработан Станиславом Рожко</p>
            </footer>
        )
    }
}

export default Footer;