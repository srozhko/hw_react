import React, { Component } from "react";
import Card from "./Card";

import './Cards.scss'


class Cards extends Component {
    render() {
        const {cards, toogleStar, showModal} = this.props

      
        const newCard = cards.map((card) => < Card cardProps={card} showModal={showModal} toogleStar={toogleStar}/>)
            return(
                <div className="cards">
                    {newCard}
                </div> 
        )
    }
}

export default Cards;