import React, { useState, useEffect, Component } from "react";
import { AiTwotoneStar } from "react-icons/ai";
import Button from "../Button";
import Modal from "../Modal";

class Card extends Component {
    state={
        star: false,
    }

    activateStar = () => {
        this.setState({star:true})
    }
    
    deactivateStar = () => {
        this.setState({star:true})
    }

    render() {
        const {star} = this.state
        const {cardProps, toogleStar, showModal} = this.props
        const {id, img, name, price, color, activeStar} = cardProps;
        // console.log(cardProps);

        return(
            <>
                <div className="card" key={id}>
                    <div className="img-wrap">
                        <img src={img} alt={name} />    
                    </div>
                    <div className="name">{name}</div>
                    <div className="price">{price}$</div>
                    <div className="color">Color: {color}</div>
                    <AiTwotoneStar className={activeStar ? "star active":"star"} onClick={()=>{toogleStar(cardProps); activeStar ? this.deactivateStar() : this.activateStar()}}/>
                    <Button text ="Додати в кошик" backgroundColor="red" onClick={()=>showModal(cardProps)}/>
                </div>
            </>
            
        )
    }
}
export default Card;